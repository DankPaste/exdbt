const Discord = require("discord.js");
const fs = require("fs");
const randomstring = require("randomstring");

let invites = require("../invites.json");

module.exports.run = async (bot, message, args) => {
  //!pay @isatisfied 59345

 //if(!message.member.roles.find("name", "Invited") || (!message.member.roles.find("name", "Beta")|| (!message.member.roles.find("name", "Developer")))){
  // return;
 // }

if(args[0] == "help"){
      message.reply("Usage: !redeeminv");
      return;
    }

  let sinvites = invites[message.author.id].invites;

  if(sinvites <= 0) return message.reply("You do not have any unused invites to redeem");

  invites[message.author.id] = {
    invites: sinvites - 1
  };

  message.channel.send(`${message.author} has redeemed an invite!`);
  //message.author.sendMessage(`Forum invite: ${randomstring.generate()}, One use Discord Invite: `);
  message.guild.channels.get('493084668476784654').createInvite({maxAge:0, maxUses: 1,unique: true, reason: `${message.author} redeemed an invite`})
    .then(invite =>
    message.author.send(`Forum invite: ${randomstring.generate()}, One use Discord invite: ${invite.url}`)
);
}

module.exports.help = {
  name: "redeeminv"
}
