const Discord = require("discord.js");
const botconfig = require("../botconfig.json");
const red = botconfig.red;
const green = botconfig.green;
const orange = botconfig.orange;
const errors = require("../utils/errors.js");

module.exports.run = async (bot, message, args) => {
    message.delete();
    if(args[0] == "help"){
      message.reply("Usage: !report <game> <date> <type>");
      return;
    }

    let reportEmbed = new Discord.RichEmbed()
    .setDescription("[Overflow] - Ban Report")
    .setColor(orange)
    .addField("Reported By", `${message.author} with ID: ${message.author.id}`)
    .addField("Game:", `${args[0]}`)
    .addField("Date:", `${args[1]}`)
    .addField("Ban-Type:", `${args[2]}`);

    let reportschannel = message.guild.channels.find(`name`, "staff");
    if(!reportschannel) return message.channel.send("Couldn't find reports channel.");
    reportschannel.send(reportEmbed);
    message.channel.send(`${message.author}, Your report has been pushed to the staff channel and we'll DM you if we need further info. Thanks for reporting.`)
}

module.exports.help = {
  name: "report"
}
