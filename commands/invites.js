const Discord = require("discord.js");
const fs = require("fs");
let invites = require("../invites.json");

module.exports.run = async (bot, message, args) => {
if(args[0] == "help"){
      message.reply("Usage: !addinv <user> <num_of_invites>");
      return;
    }
    
 if(!message.member.roles.find("name", "Main Developer")){
    return;
  }

  let pUser = message.guild.member(message.mentions.users.first()) || message.guild.members.get(args[0]);

  if(!invites[pUser.id]){
    invites[pUser.id] = {
      invites: 0
    };
  }

  let pinvites = invites[pUser.id].invites;
  let sinvites = invites[message.author.id].invites;

  invites[pUser.id] = {
    invites: pinvites + parseInt(args[1])
  };

  pUser.send(`You were given ${args[1]} invites to Overflow for your friends, If an user you invited is caught breaking the rules you may also be banned by association. Invites never expire and can only be redeemed one by one by going into #bots and using !redeeminvite`);

  fs.writeFile("./invites.json", JSON.stringify(invites));


}

module.exports.help = {
  name: "addinv"
}
